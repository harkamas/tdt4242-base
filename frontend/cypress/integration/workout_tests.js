describe("Test create workout: Assign athlete functionality", () => {
  beforeEach(() => {
    cy.loginAsCoach();
    Cypress.Cookies.preserveOnce("access", "refresh");
  });
// This test checks if its possible to create a workout without assigned athlete
  it("Test new workout with no assigned athlete", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("NoAssigned");
    cy.get("#inputDateTime").type("2021-04-05T11:11");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("text");


    cy.get("#customFile").attachFile("./workoutfile.txt");
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Test new workout with assigned athlete", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("AssignedAthlete");
    cy.get("#inputDateTime").type("2021-04-05T11:11");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("text");
    cy.get("#searchbar").type("TestAthlete")

    cy.get("#customFile").attachFile("./workoutfile.txt");
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

  it("Test new workout with assigned user not added", () => {
    cy.get("#btn-create-workout").click();

    cy.get("#inputName").type("UserNotAssigned");
    cy.get("#inputDateTime").type("2021-04-05T11:11");
    cy.get("#inputVisibility").select("PU");
    cy.get("#inputNotes").type("text");
    cy.get("#searchbar").type("TestNormalUser")


    cy.get("#customFile").attachFile("./workoutfile.txt");
    cy.scrollTo("bottom");

    cy.get("#div-exercises").within(($form) => {
      cy.get('select[name="type"]').select("1", { force: true });
      cy.get('input[name="sets"]').type("1", { force: true });
      cy.get('input[name="number"]').type("1", { force: true });
    });

    cy.get("#btn-ok-workout").click();
    cy.url().should("include", "/workouts.html");
  });

// This test should give no assigned athlete, since the user does not have a coach
  it('Test if workout with user is not assigned', () => {
    cy.visit('/workouts.html')

    cy.contains("UserNotAssigned").click();


    cy.get('#inputName').should('exist')
    cy.get('#inputDateTime').should('exist')
    cy.get('#inputOwner').should('exist')
    cy.get('#inputVisibility').should('exist')
    cy.get('#inputNotes').should('exist')
    cy.get('#uploaded-files').should('exist')
    cy.get('#div-comment-row').should('exist')
    cy.get('input[name="searchbar_value"]').invoke('val').should('be.empty')

    })

      it('Test no assigned athlete does not have athlete assigned', () => {
    cy.visit('/workouts.html')

    cy.contains("NoAssigned").click();


    cy.get('#inputName').should('exist')
    cy.get('#inputDateTime').should('exist')
    cy.get('#inputOwner').should('exist')
    cy.get('#inputVisibility').should('exist')
    cy.get('#inputNotes').should('exist')
    cy.get('#uploaded-files').should('exist')
    cy.get('#div-comment-row').should('exist')
    cy.get('input[name="searchbar_value"]').invoke('val').should('be.empty')

    })

  it('Test if assigned athlete is assigned to workout', () => {
    cy.visit('/workouts.html')

    cy.contains("AssignedAthlete").click();

    cy.get('#inputName').should('exist')
    cy.get('#inputDateTime').should('exist')
    cy.get('#inputOwner').should('exist')
    cy.get('#inputVisibility').should('exist')
    cy.get('#inputNotes').should('exist')
    cy.get('#uploaded-files').should('exist')
    cy.get('#div-comment-row').should('exist')
    cy.get('input[name="searchbar_value"]').invoke('val').should('eq', 'TestAthlete')

    })


});

describe("Test edit workout: Athlete", () => {
  beforeEach(() => {
    cy.loginAsAthlete();
    Cypress.Cookies.preserveOnce("access", "refresh");
  });

  it('Athlete does not enter completion date', () => {
    cy.visit('/workouts.html')

    cy.contains("AssignedAthlete").click();
    cy.get("#btn-edit-workout").click();
    cy.get("#btn-ok-workout").click();
    cy.get('#btn-complete-workout').should('not.be.checked')

    cy.url().should("include", "/workout.html");


  })

  it('Athlete does not enter completion date, but checks the completion box', () => {
    cy.visit('/workouts.html')

    cy.contains("AssignedAthlete").click();
    cy.get("#btn-edit-workout").click();
    cy.get("#btn-ok-workout").click();
    cy.get('#btn-complete-workout').should('not.be.checked')
    cy.get('#btn-complete-workout').check();

    cy.url().should("include", "/workout.html");

    cy.reload()
    cy.get("#btn-edit-workout").click();
    cy.get('#btn-complete-workout').should('not.be.checked')


  })

    it('Athlete enter completion date, but does not check the box', () => {
    cy.visit('/workouts.html')

    cy.contains("AssignedAthlete").click();
    cy.get("#btn-edit-workout").click();
    cy.get("#inputDateTimeCompletion").type("2021-04-05T11:11");
    cy.get("#btn-ok-workout").click();
    cy.reload();
    cy.wait(500)
    cy.get("#btn-edit-workout").click();
    cy.get('#btn-complete-workout').should('be.checked')


    cy.url().should("include", "/workout.html");

    })

    it('Athlete enter completion date, and checks the box', () => {
    cy.visit('/workouts.html')

    cy.contains("AssignedAthlete").click();
    cy.get("#btn-edit-workout").click();
    cy.get("#inputDateTimeCompletion").type("2021-04-05T11:11");
    cy.get('#btn-complete-workout').check();
    cy.get("#btn-ok-workout").click();
    cy.reload();
    cy.wait(500)
    cy.get("#btn-edit-workout").click();
    cy.get('#btn-complete-workout').should('be.checked')


    cy.url().should("include", "/workout.html");

    })

});