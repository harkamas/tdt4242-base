function makeNavLinkActive(id) {
  let link = document.getElementById(id);
  link.classList.add('active');
  link.setAttribute('aria-current', 'page');
}

function isUserAuthenticated() {
  return getCookieValue('access') != null || getCookieValue('refresh') != null;
}

function updateNavBar() {
  let nav = document.querySelector('nav');

  // Emphasize link to current page
  if (
    window.location.pathname == '/' ||
    window.location.pathname == '/index.html'
  ) {
    makeNavLinkActive('nav-index');
  } else if (window.location.pathname == '/workouts.html') {
    makeNavLinkActive('nav-workouts');
  } else if (window.location.pathname == '/exercises.html') {
    makeNavLinkActive('nav-exercises');
  } else if (window.location.pathname == '/mycoach.html') {
    makeNavLinkActive('nav-mycoach');
  } else if (window.location.pathname == '/myathletes.html') {
    makeNavLinkActive('nav-myathletes');
  }

  if (isUserAuthenticated()) {
    document.getElementById('btn-logout').classList.remove('hide');

    document.querySelector('a[href="logout.html"').classList.remove('hide');
    document.querySelector('a[href="workouts.html"').classList.remove('hide');
    document.querySelector('a[href="mycoach.html"').classList.remove('hide');
    document.querySelector('a[href="exercises.html"').classList.remove('hide');
    document.querySelector('a[href="myathletes.html"').classList.remove('hide');
  } else {
    document.getElementById('btn-login-nav').classList.remove('hide');
    document.getElementById('btn-register').classList.remove('hide');
  }
}

function setCookie(name, value, maxage, path = '') {
  document.cookie = `${name}=${value}; max-age=${maxage}; path=${path}`;
}

function deleteCookie(name) {
  setCookie(name, '', 0, '/');
}

function getCookieValue(name) {
  let cookieValue = null;
  let cookieByName = document.cookie
    .split('; ')
    .find((row) => row.startsWith(name));

  if (cookieByName) {
    cookieValue = cookieByName.split('=')[1];
  }

  return cookieValue;
}

async function sendRequest(
  method,
  url,
  body,
  contentType = 'application/json; charset=UTF-8'
) {
  if (body && contentType.includes('json')) {
    body = JSON.stringify(body);
  }

  let myHeaders = new Headers();

  if (contentType) myHeaders.set('Content-Type', contentType);
  if (getCookieValue('access'))
    myHeaders.set('Authorization', 'Bearer ' + getCookieValue('access'));
  let myInit = { headers: myHeaders, method: method, body: body };
  let myRequest = new Request(url, myInit);

  let response = await fetch(myRequest);
  if (response.status == 401 && getCookieValue('refresh')) {
    // access token not accepted. getting refresh token
    myHeaders = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
    });
    let tokenBody = JSON.stringify({ refresh: getCookieValue('refresh') });
    myInit = { headers: myHeaders, method: 'POST', body: tokenBody };
    myRequest = new Request(`${HOST}/api/token/refresh/`, myInit);
    response = await fetch(myRequest);

    if (response.ok) {
      // refresh successful, received new access token
      let data = await response.json();
      setCookie('access', data.access, 86400, '/');

      let myHeaders = new Headers({
        Authorization: 'Bearer ' + getCookieValue('access'),
        'Content-Type': contentType,
      });
      let myInit = { headers: myHeaders, method: method, body: body };
      let myRequest = new Request(url, myInit);
      response = await fetch(myRequest);

      if (!response.ok) window.location.replace('logout.html');
    }
  }

  return response;
}

function setReadOnly(readOnly, selector) {
  let form = document.querySelector(selector);
  let formData = new FormData(form);

  for (let key of formData.keys()) {
    let selector = `input[name="${key}"], textarea[name="${key}"]`;
    for (let input of form.querySelectorAll(selector)) {
      if (!readOnly && input.hasAttribute('readonly')) {
        input.readOnly = false;
      } else if (readOnly && !input.hasAttribute('readonly')) {
        input.readOnly = true;
      }
    }

    selector = `input[type="file"], select[name="${key}`;
    for (let input of form.querySelectorAll(selector)) {
      if (!readOnly && input.hasAttribute('disabled')) {
        input.disabled = false;
      } else if (readOnly && !input.hasAttribute('disabled')) {
        input.disabled = true;
      }
    }
  }

  for (let input of document.querySelectorAll(
    'input:disabled, select:disabled'
  )) {
    if (
      (!readOnly && input.hasAttribute('disabled')) ||
      (readOnly && !input.hasAttribute('disabled'))
    ) {
      input.disabled = !input.disabled;
    }
  }
}

async function getCurrentUser() {
  let user = null;
  let response = await sendRequest('GET', `${HOST}/api/users/?user=current`);
  if (!response.ok) {
    console.log('COULD NOT RETRIEVE CURRENTLY LOGGED IN USER');
  } else {
    let data = await response.json();
    user = data.results[0];
  }

  return user;
}

function createAlert(header, data) {
  let alertDiv = document.createElement('div');
  alertDiv.className = 'alert alert-warning alert-dismissible fade show';
  alertDiv.setAttribute('role', 'alert');

  let strong = document.createElement('strong');
  strong.innerText = header;
  alertDiv.appendChild(strong);

  let button = document.createElement('button');
  button.type = 'button';
  button.className = 'btn-close';
  button.setAttribute('data-bs-dismiss', 'alert');
  button.setAttribute('aria-label', 'Close');
  alertDiv.appendChild(button);

  let ul = document.createElement('ul');
  if ('detail' in data) {
    let li = document.createElement('li');
    li.innerText = data['detail'];
    ul.appendChild(li);
  } else {
    for (let key in data) {
      let li = document.createElement('li');
      li.innerText = key;

      let innerUl = document.createElement('ul');
      for (let message of data[key]) {
        let innerLi = document.createElement('li');
        innerLi.innerText = message;
        innerUl.appendChild(innerLi);
      }
      li.appendChild(innerUl);
      ul.appendChild(li);
    }
  }
  alertDiv.appendChild(ul);

  return alertDiv;
}

async function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener('input', function (e) {
    var a,
      b,
      i,
      val = this.value;
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) {
      return false;
    }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement('DIV');
    a.setAttribute('id', this.id + 'autocomplete-list');
    a.setAttribute('class', 'autocomplete-items');
    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < arr.length; i++) {
      /*check if the item starts with the same letters as the text field value:*/
      if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
        /*create a DIV element for each matching element:*/
        b = document.createElement('DIV');
        /*make the matching letters bold:*/
        b.innerHTML = '<strong>' + arr[i].substr(0, val.length) + '</strong>';
        b.innerHTML += arr[i].substr(val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener('click', function (e) {
          /*insert the value for the autocomplete text field:*/
          inp.value = this.getElementsByTagName('input')[0].value;
          /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
          closeAllLists();
        });
        a.appendChild(b);
      }
    }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener('keydown', function (e) {
    var x = document.getElementById(this.id + 'autocomplete-list');
    if (x) x = x.getElementsByTagName('div');
    if (e.keyCode == 40) {
      /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
      currentFocus++;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 38) {
      //up
      /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
      currentFocus--;
      /*and and make the current item more visible:*/
      addActive(x);
    } else if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = x.length - 1;
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add('autocomplete-active');
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove('autocomplete-active');
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName('autocomplete-items');
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener('click', function (e) {
    closeAllLists(e.target);
  });
}

//used to set values for goals in mycoach and myathlete
async function setValues(goals, user, templateGoals, navTabContent, page) {
  let goalUser;
  let showUser;
  for (let goal of goals) {
    if (page == 'mycoach.html') {
      goalUser = goal.athlete;
      showUser = goal.coach;
    } else {
      goalUser = goal.coach;
      showUser = goal.athlete;
    }
    if (user.url == goalUser) {
      let response = await sendRequest('GET', showUser);
      let otherUser = await response.json();

      // create unique id´s and append elements to HTML
      let cloneGoal = templateGoals.content.firstElementChild.cloneNode(true);
      cloneGoal.id = `goal-list-${goal.id}`;

      navTabContent.appendChild(cloneGoal);

      let selector = document.querySelector(`#goal-list-${goal.id}`);

      let newGoal = selector.querySelector('#outputGoal');
      newGoal.id = `outputGoal-${goal.id}`;

      let creationDate = selector.querySelector('#creationDate');
      creationDate.id = `creationDate-${goal.id}`;

      let completion_date = selector.querySelector('#completion_date');
      completion_date.id = `completion_date-${goal.id}`;

      let otherUserField = selector.querySelector('#otherUser');
      otherUserField.id = `otherUser-${goal.id}`;

      let creationVal = new Date(goal.creation_date);
      let newVal = creationVal.toLocaleDateString();

      // Set field values
      if (goal.completed) {
        let completionVal = new Date(goal.completion_date);
        let date = completionVal.toLocaleDateString();
        completion_date.value = date;
      }

      newGoal.value = goal.goal;
      creationDate.value = newVal;
      otherUserField.value = otherUser.username;

      if (page == 'mycoach.html') {
        let check = selector.querySelector('#checkCompleted');
        check.id = `checkCompleted-${goal.id}`;

        check.checked = goal.completed;

        check.addEventListener('change', async () => updateCompleted(goal));
      } else {
        let completed = selector.querySelector('#completed');
        completed.id = `completed-${goal.id}`;
        completed.value = goal.completed;
      }
    }
  }
}
