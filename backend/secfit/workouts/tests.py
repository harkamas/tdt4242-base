"""
Tests for the workouts application.
"""
from django.test import TestCase, Client
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate
from rest_framework.request import Request
from .permissions import (
    IsOwner,
    IsOwnerOfWorkout,
    IsPublic,
    IsReadOnly,
    IsCoachAndVisibleToCoach,
    IsCoachOfWorkoutAndVisibleToCoach,
    IsWorkoutPublic,
    IsAssignedWorkoutForAthlete,
)
from workouts.models import Workout, Exercise, ExerciseInstance
from users.models import User
from .views import ExerciseInstanceList
from datetime import date


class TestPermissions(APITestCase):
    """Class for testing application permissions"""

    def create_workout(self, name, athlete, visibility):

        return Workout.objects.create(
            name=name,
            date=date.today(),
            notes="notes",
            owner=athlete,
            athlete=athlete,
            visibility=visibility,
            completed=False,
        )

    def create_owner(self):

        user = User.objects.create(
            username="coach",
            email="coach@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )

        user.set_password("test")
        user.save()
        return user

    def create_athlete(self):

        user = User.objects.create(
            username="athlete",
            email="athlete@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )
        user.set_password("test")
        user.save()
        return user

    def create_user(self):

        user = User.objects.create(
            username="user",
            email="user@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )
        user.set_password("test")
        user.save()
        return user

    def create_exercise(self, name):
        return Exercise.objects.create(name=name, description="this is", unit="5")

    def create_exercise_instance(self, Exercise, Workout):
        return ExerciseInstance.objects.create(
            workout=Workout, exercise=Exercise, sets="5", number="4"
        )

    def setUp(self):
        self.client = Client()
        o = self.create_owner()
        self.assertTrue(isinstance(o, User))

        a = self.create_athlete()
        self.assertTrue(isinstance(a, User))
        a.coach = o
        a.save()

        u = self.create_user()
        self.assertTrue(isinstance(u, User))

        e1 = self.create_exercise("Pushups")
        e2 = self.create_exercise("Situps")
        self.assertTrue(isinstance(e1, Exercise))
        self.assertTrue(isinstance(e2, Exercise))

        w1 = self.create_workout("test1", a, "PU")
        w2 = self.create_workout("test2", a, "CO")
        self.assertTrue(isinstance(w1, Workout))
        self.assertTrue(isinstance(w2, Workout))

        self.create_exercise_instance(e1, w1)
        self.create_exercise_instance(e2, w2)

    def test_IsOwner(self):
        request = APIRequestFactory().get("/")
        athlete = User.objects.get(username="athlete")
        workout = Workout.objects.get(name="test1")
        request.user = athlete

        resp = IsOwner.has_object_permission(
            self, request=request, view=None, obj=workout
        )

        self.assertTrue(resp)

    def test_IsAssignedWorkoutForAthlete(self):
        request = APIRequestFactory().get("/")
        athlete = User.objects.get(username="athlete")
        workout = Workout.objects.get(name="test1")

        self.assertTrue(resp)

    def test_IsCoachAndvisibleToCoach(self):
        request = APIRequestFactory().get("/")
        coach = User.objects.get(username="coach")
        workout = Workout.objects.get(name="test2")

        request.user = coach

        resp = IsCoachAndVisibleToCoach.has_object_permission(
            self, request=request, view=None, obj=workout
        )

        self.assertTrue(resp)

    def test_IsPublic(self):
        request = APIRequestFactory().get("/")
        user = User.objects.get(username="user")
        workout = Workout.objects.get(name="test1")

        request.user = user

        resp = IsPublic.has_object_permission(
            self, request=request, view=None, obj=workout
        )

        self.assertTrue(resp)

    def test_IsWorkoutPublic(self):

        exercise = ExerciseInstance.objects.get(exercise=1)

        resp = IsWorkoutPublic.has_object_permission(
            self, request=None, view=None, obj=exercise
        )

        self.assertTrue(resp)

    def test_IsWorkoutOwnerTrue(self):
        request = APIRequestFactory().get("/")
        athlete = User.objects.get(username="athlete")
        workout = Workout.objects.get(name="test1")
        exercise = ExerciseInstance.objects.get(exercise=1)

        request.user = athlete
        request.method = "POST"
        request.data = {"workout": "http://127.0.0.1:8000/api/workouts/1/"}

        has_perm = IsOwnerOfWorkout.has_permission(self, request=request, view=None)

        self.assertTrue(has_perm)

    def test_IsWorkoutOwnerObj(self):
        request = APIRequestFactory().get("/")
        athlete = User.objects.get(username="athlete")
        workout = Workout.objects.get(name="test1")
        exercise = ExerciseInstance.objects.get(exercise=1)

        request.user = athlete
        request.method = "GET"

        resp = IsOwnerOfWorkout.has_object_permission(
            self, request=request, view=None, obj=exercise
        )
        self.assertTrue(resp)

    def test_IsWorkoutOwnerFalse(self):
        request = APIRequestFactory().get("/")
        athlete = User.objects.get(username="user")
        workout = Workout.objects.get(name="test1")
        exercise = Exercise.objects.get(name="Pushups")

        request.user = athlete
        request.method = "POST"
        request.data = {}

        has_perm = IsOwnerOfWorkout.has_permission(self, request=request, view=None)
        self.assertFalse(has_perm)

    def test_IsReadOnly(self):
        request = APIRequestFactory().get("/workout-detail/2")
        athlete = User.objects.get(username="user")
        workout = ExerciseInstance.objects.get(exercise=2)

        request.user = athlete

        resp = IsReadOnly.has_object_permission(
            self, request=request, view=None, obj=workout
        )

        self.assertTrue(resp)

    def test_IsCoachOfWorkoutAndVisibleToCoach(self):
        request = APIRequestFactory().get("/")
        coach = User.objects.get(username="coach")
        workout = ExerciseInstance.objects.get(exercise=2)

        request.user = coach

        resp = IsCoachOfWorkoutAndVisibleToCoach.has_object_permission(
            self=IsCoachOfWorkoutAndVisibleToCoach,
            request=request,
            view=None,
            obj=workout,
        )

        self.assertTrue(resp)
