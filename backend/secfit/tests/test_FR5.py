from django.test import TestCase, Client
from rest_framework.reverse import reverse
from allpairspy import AllPairs
from workouts.models import Workout, Exercise, ExerciseInstance
from users.models import User
from comments.models import Comment
from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate
from rest_framework.request import Request
from workouts.views import WorkoutDetail
from comments.views import CommentList
from datetime import date
from django.db import models
import json


class FR5(TestCase):
    def create_workout(self, name, athlete, visibility):

        return Workout.objects.create(
            name=name,
            date=date.today(),
            notes="notes",
            owner=athlete,
            athlete=athlete,
            visibility=visibility,
            completed=False,
        )

    def create_owner(self):

        user = User.objects.create(
            username="coach",
            email="coach@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )

        user.set_password("test")
        user.save()
        return user

    def create_athlete(self):

        user = User.objects.create(
            username="athlete",
            email="athlete@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )
        user.set_password("test")
        user.save()
        return user

    def create_user(self):

        user = User.objects.create(
            username="user",
            email="user@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )
        user.set_password("test")
        user.save()
        return user

    def create_exercise(self, name):
        return Exercise.objects.create(name=name, description="this is", unit="5")

    def create_exercise_instance(self, Exercise, Workout):

        return ExerciseInstance.objects.create(
            workout=Workout, exercise=Exercise, sets="5", number="4"
        )

    def create_comment(self, owner, workout, text):
        return Comment.objects.create(owner=owner, workout=workout, content=text)

    def setUp(self):
        self.client = Client()
        o = self.create_owner()
        self.assertTrue(isinstance(o, User))
        a.save()

        self.create_exercise_instance(e1, publicW)
        self.create_exercise_instance(e2, coachW)

        self.create_comment(a, publicW, "This is a public comment")
        self.create_comment(a, coachW, "This is a coach comment")
        self.create_comment(a, privateW, "This is a private comment")

    def test_owner_sees(self):
        factory = APIRequestFactory()
        user = User.objects.get(username="athlete")
        view = WorkoutDetail.as_view()
        comment_view = CommentList.as_view()

        # try comments
        comment_request = factory.get("api/comments/")
        force_authenticate(comment_request, user=user)
        self.assertTrue(comment_response.data["count"] == 3)

        # try public
        request = factory.get("api/workout-detail/1")
        force_authenticate(request, user=user)
        response = view(request, pk=1)
        self.assertTrue(response.data["completed"] == False)

        # try coach
        request = factory.get("api/workout-detail/2")
        force_authenticate(request, user=user)
        response = view(request, pk=2)
        self.assertTrue(response.data["visibility"] == "CO")

        # try private
        request = factory.get("api/workout-detail/3")
        force_authenticate(request, user=user)
        response = view(request, pk=3)
        self.assertEqual(response.status_code, 200)

        self.assertTrue(response.data["notes"] == "notes")
        self.assertTrue(response.data["visibility"] == "PR")
        self.assertTrue(response.data["completed"] == False)

    def test_coach_sees(self):
        factory = APIRequestFactory()
        user = User.objects.get(username="coach")
        view = WorkoutDetail.as_view()
        self.assertTrue(response.data["visibility"] == "PU")
        self.assertTrue(response.data["completed"] == False)

        # try coach
        request = factory.get("api/workout-detail/2")
        response = view(request, pk=2)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data["notes"] == "notes")
        self.assertTrue(response.data["visibility"] == "CO")
        self.assertTrue(response.data["completed"] == False)

        # try private
        request = factory.get("api/workout-detail/3")
        response = view(request, pk=3)
        self.assertEqual(response.status_code, 400)

    def test_user_sees(self):
        factory = APIRequestFactory()
        user = User.objects.get(username="user")
        view = WorkoutDetail.as_view()
        comment_view = CommentList.as_view()

        # try comments
        comment_request = factory.get("api/comments/")
        force_authenticate(comment_request, user=user)
        comment_response = comment_view(comment_request)
        self.assertTrue(comment_response.data["count"] == 1)

        # try public
        request = factory.get("api/workout-detail/1")
        force_authenticate(request, user=user)
        response = view(request, pk=1)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data["notes"] == "notes")
        self.assertTrue(response.data["visibility"] == "PU")
        self.assertTrue(response.data["completed"] == False)

        # try coach
        request = factory.get("api/workout-detail/2")
        force_authenticate(request, user=user)
        response = view(request, pk=2)
        self.assertEqual(response.status_code, 403)

        # try private
        request = factory.get("api/workout-detail/3")
        force_authenticate(request, user=user)
        response = view(request, pk=3)
        self.assertEqual(response.status_code, 403)
