from django.test import TestCase, Client
from unittest import skip
from rest_framework.reverse import reverse
from workouts.models import Workout, Exercise, ExerciseInstance
from users.models import User
from rest_framework.test import APITestCase, APIRequestFactory, force_authenticate
from rest_framework.request import Request
from datetime import date


class BoundaryTestingRegister(TestCase):
    def test_register_username_maximum(self):
        # Testing if user can be created with >150 char as username
        # Returns ok, passed
        user151char = "a" * 151

        code = 400

        data = {
            "username": user151char,
            "email": "test@testing.no",
            "password": "test",
            "password1": "test",
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }

        resp = self.client.post(reverse("user-list"), data)
        self.assertEqual(resp.status_code, code)

    def test_register_username_minimum(self):
        # Testing if user can be created with <=0 char as username
        # Returns ok, passed
        user0char = ""

        code = 400

        data = {
            "username": user0char,
            "email": "test@testing.no",
            "password": "test",
            "password1": "test",
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }

        resp = self.client.post(reverse("user-list"), data)
        self.assertEqual(resp.status_code, code)

    def test_register_email_without_at_sign(self):
        # Testing if user can be created without @ in email

        email_without_at_sign = "testing.no"

        code = 400

        data = {
            "username": "test10",
            "email": email_without_at_sign,
            "password": "test",
            "password1": "test",
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }

        resp = self.client.post(reverse("user-list"), data)
        self.assertEqual(resp.status_code, code)

    def test_register_email_maximum(self):
        # Testing if user can be created with >254 char as password

        email_long_part = "test" * 255
        email_short_part = "testing.no"
        email = email_long_part + email_short_part

        code = 400

        data = {
            "username": "test11",
            "email": email,
            "password": "test",
            "password1": "test",
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }

        resp = self.client.post(reverse("user-list"), data)
        self.assertEqual(resp.status_code, code)

    from unittest import skip

    @skip(
        "Skipping test_register_password_maximum because no password validation is used in backend"
    )
    def test_register_password_maximum(self):
        # Testing if user can be created with >128 char as password

        password = "t" * 129

        code = 400

        data = {
            "username": "test12",
            "email": "test@testing.no",
            "password": password,
            "password1": password,
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }

        resp = self.client.post(reverse("user-list"), data)
        self.assertEqual(resp.status_code, code)

    def test_register_password_minimum(self):
        # Testing if user can be created with <0 char as password

        password = ""

        code = 400

        data = {
            "username": "test13",
            "email": "test@testing.no",
            "password": password,
            "password1": password,
            "phone_number": "12345678",
            "country": "testland",
            "city": "test city",
            "street_address": "test avenue",
        }

        resp = self.client.post(reverse("user-list"), data)
        self.assertEqual(resp.status_code, code)

    def test_register_phccs_maximum(self):
        # Testing if user can be created with >50 char as phone_number, country, city, street_address

        char51 = "a" * 51
        numberChar51 = "1" * 51

        code = 400

        data = {
            "username": "test14",
            "email": "test@testing.no",
            "password": "test",
            "password1": "test",
            "phone_number": numberChar51,
            "country": char51,
            "city": char51,
            "street_address": char51,
        }

        resp = self.client.post(reverse("user-list"), data)
        self.assertEqual(resp.status_code, code)


class BoundaryTestingWorkout(TestCase):
    def create_athlete(self):

        user = User.objects.create(
            username="athlete111",
            email="athlete@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )
        user.set_password("test")
        user.save()

        return user

    def create_user(self):

        user = User.objects.create(
            username="user111",
            email="user@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )
        user.set_password("test")
        user.save()
        return user

    def create_owner(self):

        user = User.objects.create(
            username="coach111",
            email="coach@mail.no",
            phone_number="12345678",
            country="testland",
            city="test city",
            street_address="test avenue",
        )

        user.set_password("test")
        user.save()
        return user

    def create_workout(self, name, athlete, visibility):

        return Workout.objects.create(
            name=name,
            date=date.today(),
            notes="notes",
            owner=athlete,
            athlete=athlete,
            visibility=visibility,
            completed=False,
        )

    def create_exercise(self, name):
        return Exercise.objects.create(name=name, description="this is", unit="5")

    def create_exercise_instance(self, Exercise, Workout):

        return ExerciseInstance.objects.create(
            workout=Workout, exercise=Exercise, sets="5", number="4"
        )

    def setUp(self):
        self.client = Client()
        o = self.create_owner()
        a = self.create_athlete()
        self.athlete_username = "athlete111"

        """ a.coach = o
        a.save()
        u = self.create_user()
        e1 = self.create_exercise("Pushups")
        w1 = self.create_workout("workout1", a, "PU")
        ei1 = self.create_exercise_instance(e1, w1) """

    def test_workout_name_maximum(self):
        # Testing if name of workout can be >150 char
        name = "test" * 151
        athlete = User.objects.get(username=self.athlete_username)
        visibility = "PU"

        Workout.objects.create(
            name=name,
            date=date.today(),
            notes="notes",
            owner=athlete,
            athlete=athlete,
            visibility=visibility,
            completed=False,
        )

        workout = Workout.objects.get(name=name)
        self.assertFalse(len(workout.name) == 151)

    def test_workout_visibility_maximum(self):
        # Testing if visibility length can be more than 2
        name = "workout111"
        visibility = "a" * 3
        athlete = User.objects.get(username=self.athlete_username)

        Workout.objects.create(
            name=name,
            date=date.today(),
            notes="notes",
            owner=athlete,
            athlete=athlete,
            visibility=visibility,
            completed=False,
        )

        workout = Workout.objects.get(name=name)
        self.assertTrue(workout.visibility == visibility)

    def test_workout_exercise_name_maximum(self):
        # Testing if exercise name length can be more than 100
        name = "test" * 101
        e1 = self.create_exercise(name)

        exercise = Exercise.objects.get(name=name)
        self.assertFalse(len(exercise.name) == 101)

    def test_workout_exercise_unit_maximum(self):
        # Testing if exercise unit length can be more than 50
        name = "test" * 10
        unit = "1" * 51
        Exercise.objects.create(name=name, description="this is", unit=unit)

        exercise = Exercise.objects.get(name=name)
        self.assertTrue(len(exercise.unit) == 51)
